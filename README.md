<img src="https://gitlab.com/dividebyzer0/coordinate-calculator/badges/master/pipeline.svg"/>

# Coordinate Calculator

This Android application is a coordinate calculation helper for *Signal Simulator*.

https://store.steampowered.com/app/839310/Signal_Simulator/

Inspired by dcay's SSCC Javascript web application.

https://dcay.github.io/Signal-Simulator-Coordinates-Calculator/

<img src="http://dividebyzer0.gitlab.io/images/Screenshot_20200104-104618.png" style="zoom:20%;" />

## Installation

Download the APK located in the root directory of this repo, install by clicking on the downloaded APK file.

If you'd like to build from source, simply `git clone` this repo and import the project into Android Studio.

## Usage

1. Upon investigating a signal and obtaining a "ticker" of coordinates, make note of:

- The **lowest** *azimuth* seen
- The **highest** *azimuth* seen
- The **lowest** *elevation* seen
- The **highest** *elevation* seen

2. Input each respective value in the app.
3. Press "CALCULATE"

> This calculation is done using the formula found on the corkboard inside the main office. 
>
> By monitoring the flow of data into the buffer for ~1 minute (typically while the antennas are rotating into the general area of the signal) and tweaking the inputs as needed, the resulting azimuth and elevation should be accurate within ~0.5 degrees of arc. Achieving accuracy inside ~0.001 degrees of arc, with signal strengths of 99%+ is possible quite regularly.

Press "CLEAR ALL" to quickly clear all values to their default (null) values.

## Requirements

Android OS 9.0 or later

(working on compatibility for older devices)



## Build Requirements

If you'd like to build the app yourself, I recommend **Compile SDK Version** API 28 Android 9.0 and **Build Tools Version** *29.0.2*



## Issues

Feel free to report any issues (or feature recommendations). 

Known issues include:

- ~~No checks for improper coordinates (eg, 360.1+ degree azimuth)~~
- No checks for length of coordinates (will implement check for max 5 decimal place precision)
- Application not signed (packaged here as debug)
