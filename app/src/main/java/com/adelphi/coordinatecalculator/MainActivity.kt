package com.adelphi.coordinatecalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.VisibleForTesting
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Fixed vals for both UI button elements
        val buttonClear = findViewById<Button>(R.id.buttonClear)
        val buttonCalc = findViewById<Button>(R.id.buttonCalc)

        buttonCalc.setOnClickListener {

            // Vars for all arithmetic operations
            val lowAz = findViewById<EditText>(R.id.lowAz).getText().toString().toDoubleOrNull() ?: return@setOnClickListener
            val lowEl = findViewById<EditText>(R.id.lowEl).getText().toString().toDoubleOrNull() ?: return@setOnClickListener
            val highAz = findViewById<EditText>(R.id.highAz).getText().toString().toDoubleOrNull() ?: return@setOnClickListener
            val highEl = findViewById<EditText>(R.id.highEl).getText().toString().toDoubleOrNull() ?: return@setOnClickListener
            val resultAz:TextView = findViewById<TextView>(R.id.resultAz)
            val resultEl:TextView = findViewById<TextView>(R.id.resultEl)

            // Calcs from corkboard (very accurate!)
            val azrangeA:Double = (lowAz + 20)
            val azrangeB:Double = (highAz - 20)
            val elrangeA:Double = (lowEl + 20)
            val elrangeB:Double = (highEl - 20)
            val calcAz:Double = ((azrangeB + azrangeA) / 2.0)
            val calcEl:Double = ((elrangeA + elrangeB) / 2.0)

            // Print results

            if (lowAz > 360 || highAz > 360) {
                Toast.makeText(this@MainActivity, "Invalid azimuth value", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else {
                resultAz.text = "%.5f".format(calcAz).toString()
            }
            if (lowEl > 90 || highEl > 90) {
                Toast.makeText(this@MainActivity, "Invalid elevation value", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            } else {
                resultEl.text = "%.5f".format(calcEl).toString()
            }

        }

        buttonClear.setOnClickListener{

            // Clear all fields to default vals
            lowAz.getText().clear()
            lowEl.getText().clear()
            highAz.getText().clear()
            highEl.getText().clear()
            resultAz.setText("")
            resultEl.setText("")
        }

    }
}

