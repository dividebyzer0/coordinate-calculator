package com.adelphi.coordinatecalculator

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Fixed vals for both UI button elements
        val buttonClear = findViewById<Button>(R.id.buttonClear)
        val buttonCalc = findViewById<Button>(R.id.buttonCalc)

        buttonCalc.setOnClickListener {

            // Vars for all arithmetic operations
            var lowAz = findViewById<EditText>(R.id.lowAz).getText().toString().toDoubleOrNull() ?: return@setOnClickListener
            var lowEl = findViewById<EditText>(R.id.lowEl).getText().toString().toDoubleOrNull() ?: return@setOnClickListener
            var highAz = findViewById<EditText>(R.id.highAz).getText().toString().toDoubleOrNull() ?: return@setOnClickListener
            var highEl = findViewById<EditText>(R.id.highEl).getText().toString().toDoubleOrNull() ?: return@setOnClickListener
            var resultAz:TextView = findViewById<TextView>(R.id.resultAz)
            var resultEl:TextView = findViewById<TextView>(R.id.resultEl)

            // Do math
            //var calcAz:Double = ((highAz - lowAz) / 2.0) + lowAz
            //var calcEl:Double = ((highEl - lowEl) / 2.0) + lowEl

            // Calcs from corkboard (very accurate!)
            var azrangeA:Double = (lowAz + 20)
            var azrangeB:Double = (highAz - 20)
            var elrangeA:Double = (lowEl + 20)
            var elrangeB:Double = (highEl - 20)
            var calcAz:Double = ((azrangeB + azrangeA) / 2.0)
            var calcEl:Double = ((elrangeA + elrangeB) / 2.0)

            // Print results
            resultAz.text = "%.5f".format(calcAz).toString()
            resultEl.text = "%.5f".format(calcEl).toString()

        }

        buttonClear.setOnClickListener{

            // Clear all fields to default vals
            lowAz.getText().clear()
            lowEl.getText().clear()
            highAz.getText().clear()
            highEl.getText().clear()
            resultAz.setText("")
            resultEl.setText("")
        }

    }
}

